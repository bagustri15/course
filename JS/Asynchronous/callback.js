// const p1 = () => {
//   setTimeout(() => {
//     console.log("p1");
//   }, 1000);
// };

// const p2 = () => {
//   setTimeout(() => {
//     console.log("p2");
//   }, 3000);
// };

// const p3 = () => {
//   setTimeout(() => {
//     console.log("p3");
//   }, 2000);
// };

// p1();
// p2();
// p3();

const p1 = (callback) => {
  setTimeout(() => {
    console.log("p1");
    callback();
  }, 1000);
};

const p2 = (callback) => {
  setTimeout(() => {
    console.log("p2");
    callback();
  }, 3000);
};

const p3 = () => {
  setTimeout(() => {
    console.log("p3");
  }, 2000);
};

p1(() => {
  p2(() => {
    p3();
  });
});
