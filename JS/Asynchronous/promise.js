// const printerFunction = (resolve, reject) => {
//   let printed = true; // change to `false` to trigger `reject()`
//   if (printed) {
//     resolve("📃"); // 📃 resolved value
//   } else {
//     reject("⚠️"); // ⚠️ rejection reason
//   }
// };
// const printPromise = new Promise(printerFunction);

// const handleSuccess = (resolvedValue) => {
//   console.log(`Yay! Let's put ${resolvedValue} in the folder 📁!`);
// };

// const handleFailure = (rejectionReason) => {
//   console.log(
//     `Oh no! There was an error: ${rejectionReason}! Let's fix it ⚒️!`
//   );
// };

// const makePrintCompleteSound = () => {
//   console.log("Beep 🎵!");
// };

// printPromise
//   .then(handleSuccess)
//   .catch(handleFailure)
//   .finally(makePrintCompleteSound);

const p1 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // console.log("p1");
      resolve("p1");
    }, 1000);
  });
};

const p2 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // console.log("p2");
      resolve("p2");
    }, 3000);
  });
};

const p3 = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // console.log("p3");
      resolve("p3");
    }, 2000);
  });
};

p1()
  .then((result) => {
    console.log(result);
    p2().then((result) => {
      console.log(result);
      p3().then((result) => {
        console.log(result);
      });
    });
  })
  .catch((error) => {
    console.log(error);
  });
